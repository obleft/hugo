## Welcome to Mettabits

The quality of your consciousness is what determines the future.

This website is dedicated enhancing the quality of your consciousness -- which means helping you become more present for the moments of your life.
